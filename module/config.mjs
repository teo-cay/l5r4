export const l5r4 = {};

l5r4.arrows = {
  armor: "l5r4.arrows.armor",
  flesh: "l5r4.arrows.flesh",
  humming: "l5r4.arrows.humming",
  rope: "l5r4.arrows.rope",
  willow: "l5r4.arrows.willow"
}

l5r4.rings = {
  fire: "l5r4.rings.fire",
  water: "l5r4.rings.water",
  air: "l5r4.rings.air",
  earth: "l5r4.rings.earth",
  void: "l5r4.rings.void"
}

l5r4.spellRings = {
  fire: "l5r4.rings.fire",
  water: "l5r4.rings.water",
  air: "l5r4.rings.air",
  earth: "l5r4.rings.earth",
  void: "l5r4.rings.void",
  all: "l5r4.rings.all"
}

l5r4.traits = {
  sta: "l5r4.traits.sta",
  wil: "l5r4.traits.wil",
  str: "l5r4.traits.str",
  per: "l5r4.traits.per",
  ref: "l5r4.traits.ref",
  awa: "l5r4.traits.awa",
  agi: "l5r4.traits.agi",
  int: "l5r4.traits.int",
  void: "l5r4.rings.void"
}

l5r4.npcTraits = {
  sta: "l5r4.traits.sta",
  wil: "l5r4.traits.wil",
  str: "l5r4.traits.str",
  per: "l5r4.traits.per",
  ref: "l5r4.traits.ref",
  awa: "l5r4.traits.awa",
  agi: "l5r4.traits.agi",
  int: "l5r4.traits.int"
}

l5r4.skillTypes = {
  high: "l5r4.skillTypes.high",
  bugei: "l5r4.skillTypes.bugei",
  merch: "l5r4.skillTypes.merch",
  low: "l5r4.skillTypes.low"
}

l5r4.actionTypes = {
  simple: "l5r4.mech.simple",
  complex: "l5r4.mech.complex",
  free: "l5r4.mech.free"
}

l5r4.kihoTypes = {
  internal: "l5r4.kiho.internal",
  karmic: "l5r4.kiho.karmic",
  martial: "l5r4.kiho.martial",
  mystic: "l5r4.kiho.mystic"
}

l5r4.advantageTypes = {
  physical: "l5r4.advantage.physical",
  mental: "l5r4.advantage.mental",
  social: "l5r4.advantage.social",
  material: "l5r4.advantage.material",
  spiritual: "l5r4.advantage.spiritual",
  ancestor: "l5r4.advantage.ancestor"
}

l5r4.npcNumberWoundLvls = {
  1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8
}

l5r4.statusEffects = [
  { 
    id: "attackStance", name: "EFFECT.attackStance", img: "systems/l5r4/assets/icons/attackstance.png" 
  },
  { 
    id: "fullAttackStance", name: "EFFECT.fullAttackStance", img: "systems/l5r4/assets/icons/fullattackstance.png"
  },
  { 
    id: "defenseStance", name: "EFFECT.defenseStance", img: "systems/l5r4/assets/icons/defensestance.png" 
  },
  { 
    id: "fullDefenseStance", name: "EFFECT.fullDefenseStance", img: "systems/l5r4/assets/icons/fulldefensestance.png" 
  },
  { 
    id: "centerStance", name: "EFFECT.centerStance", img: "systems/l5r4/assets/icons/centerstance.png" 
  },
  { 
    id: "blinded", name: "EFFECT.blinded", img: "icons/svg/blind.svg" 
  },
  {
    id: "dazed", name: "EFFECT.dazed", img: "icons/svg/stoned.svg",
  },
  {
    id: "entangled", name: "EFFECT.entangled", img: "icons/svg/net.svg",
  },
  {
    id: "fasting", name: "EFFECT.fasting", img: "icons/svg/silenced.svg",
  },
  {
    id: "fatigued", name: "EFFECT.fatigued", img: "icons/svg/sleep.svg",
  },
  {
    id: "grappled", name: "EFFECT.grappled", img: "systems/l5r4/assets/icons/grapple.png",
  },
  {
    id: "mounted", name: "EFFECT.mounted", img: "systems/l5r4/assets/icons/mounted.png",
  },
  {
    id: "prone", name: "EFFECT.prone", img: "icons/svg/falling.svg"
  },
  {
    id: "stunned", name: "EFFECT.stunned", img: "icons/svg/daze.svg"
  }
]